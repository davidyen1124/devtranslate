package com.sparkslab.devtranslate;

import java.io.IOException;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MainActivity extends Activity implements OnClickListener,
		OnItemSelectedListener, OnPreparedListener {
	private ImageButton mMicImageButton, mTranslateImageButton;
	private Spinner mToLanguageSpinner;
	private EditText mFromTextEditText;
	private TextView mToTextTextView;
	private ProgressBar mProgressBar;
	private MediaPlayer mMediaPlayer;

	private static final String TAG = "SparksLab";
	private final int REQUEST_OK = 404;
	private final String[] mLanguageCode = { "ar", "bg", "ca", "zh-CHS",
			"zh-CHT", "cs", "da", "nl", "en", "et", "fi", "fr", "de", "el",
			"ht", "he", "hi", "mww", "hu", "id", "it", "ja", "tlh", "tlh-Qaak",
			"ko", "lv", "lt", "ms", "mt", "no", "fa", "pl", "pt", "ro", "ru",
			"sk", "sl", "es", "sv", "th", "tr", "uk", "ur", "vi", "cy" };
	private final String[] mLanguageName = { "Arabic", "Bulgarian", "Catalan",
			"Chinese Simplified", "Chinese Traditional", "Czech", "Danish",
			"Dutch", "English", "Estonian", "Finnish", "French", "German",
			"Greek", "Haitian Creole", "Hebrew", "Hindi", "Hmong Daw",
			"Hungarian", "Indonesian", "Italian", "Japanese", "Klingon",
			"Klingon (pIqaD)", "Korean", "Latvian", "Lithuanian", "Malay",
			"Maltese", "Norwegian", "Persian", "Polish", "Portuguese",
			"Romanian", "Russian", "Slovak", "Slovenian", "Spanish", "Swedish",
			"Thai", "Turkish", "Ukrainian", "Urdu", "Vietnamese", "Welsh" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initialValues();
		findViews();
		setListeners();
	}

	private void initialValues() {
		mMediaPlayer = new MediaPlayer();
	}

	private void findViews() {
		mMicImageButton = (ImageButton) findViewById(R.id.mic_imageButton);
		mTranslateImageButton = (ImageButton) findViewById(R.id.translate_imageButton);
		mToLanguageSpinner = (Spinner) findViewById(R.id.toLanguage_spinner);
		mFromTextEditText = (EditText) findViewById(R.id.fromText_editText);
		mToTextTextView = (TextView) findViewById(R.id.toText_textView);
		mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, mLanguageName);
		mToLanguageSpinner.setAdapter(adapter);
	}

	private void setListeners() {
		mMicImageButton.setOnClickListener(this);
		mTranslateImageButton.setOnClickListener(this);
		mToTextTextView.setOnClickListener(this);
		mToLanguageSpinner.setOnItemSelectedListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.mic_imageButton) {
			startSpeechRecognition();
		} else if (v.getId() == R.id.translate_imageButton) {
			startTranslate();
		} else if (v.getId() == R.id.toText_textView) {
			playTts();
		}
	}

	// play google's unofficial translate TTS API
	private void playTts() {
		String toText = mToTextTextView.getText().toString();
		if (toText.length() <= 0) {
			return;
		}

		mTranslateImageButton.setVisibility(View.GONE);
		mProgressBar.setVisibility(View.VISIBLE);

		String url = "http://translate.google.com/translate_tts?tl="
				+ mLanguageCode[mToLanguageSpinner.getSelectedItemPosition()]
				+ "&q=" + toText;

		Log.d(TAG, "translate: " + url);

		try {
			mMediaPlayer.stop();
			mMediaPlayer.reset();
			mMediaPlayer.setDataSource(url);
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mMediaPlayer.setOnPreparedListener(this);
			mMediaPlayer.prepareAsync();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();

			mTranslateImageButton.setVisibility(View.VISIBLE);
			mProgressBar.setVisibility(View.GONE);
		} catch (SecurityException e) {
			e.printStackTrace();

			mTranslateImageButton.setVisibility(View.VISIBLE);
			mProgressBar.setVisibility(View.GONE);
		} catch (IllegalStateException e) {
			e.printStackTrace();

			mTranslateImageButton.setVisibility(View.VISIBLE);
			mProgressBar.setVisibility(View.GONE);
		} catch (IOException e) {
			e.printStackTrace();

			mTranslateImageButton.setVisibility(View.VISIBLE);
			mProgressBar.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
		}
		mMediaPlayer.reset();
	}

	@Override
	protected void onStop() {
		super.onStop();

		mMediaPlayer.release();
	}

	// process translate
	private void startTranslate() {
		// if there is nothing in editText, don't use API
		if (mFromTextEditText.length() <= 0) {
			return;
		}

		// if there is no internet connection, don't use API too
		if (!isNetworkConnected(this)) {
			Toast.makeText(this, R.string.network_available, Toast.LENGTH_LONG)
					.show();
			return;
		}

		mTranslateImageButton.setVisibility(View.GONE);
		mProgressBar.setVisibility(View.VISIBLE);

		String languageCode = mLanguageCode[mToLanguageSpinner
				.getSelectedItemPosition()];
		AsyncHttpClient client = new AsyncHttpClient();

		RequestParams params = new RequestParams();
		params.put("to_language", languageCode);
		params.put("from_text", mFromTextEditText.getText().toString());

		client.post("http://sparkslab.twbbs.org/sparkslab/translater/", params,
				new JsonHttpResponseHandler() {

					@Override
					public void onSuccess(int statusCode, JSONObject response) {
						super.onSuccess(statusCode, response);

						Log.d(TAG, response.toString());

						try {
							mToTextTextView.setText(response
									.getString("to_text"));
						} catch (JSONException e) {
							e.printStackTrace();

							mToTextTextView.setText("");
						} finally {
							mTranslateImageButton.setVisibility(View.VISIBLE);
							mProgressBar.setVisibility(View.GONE);
						}
					}

					@Override
					public void onFailure(int statusCode, Header[] headers,
							String responseBody, Throwable e) {
						super.onFailure(statusCode, headers, responseBody, e);

						Toast.makeText(MainActivity.this, responseBody,
								Toast.LENGTH_LONG).show();

						mTranslateImageButton.setVisibility(View.VISIBLE);
						mProgressBar.setVisibility(View.GONE);
					}
				});
	}

	// open speech recognition dialog
	private void startSpeechRecognition() {
		Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "zh-TW");
		try {
			startActivityForResult(i, REQUEST_OK);
		} catch (Exception e) {
			Toast.makeText(this, "Error initializing speech to text engine.",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_OK && resultCode == RESULT_OK) {
			List<String> results = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			Log.i(TAG, results.toString());

			if (results != null && results.size() > 0) {
				openRecognitionSelector(results);
			} else {
				Toast.makeText(this, R.string.cant_recognize_speech,
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	// if results has more than one results, open an AlertDialog to choose.
	// Otherwise setText directly.
	private void openRecognitionSelector(final List<String> results) {
		if (results.size() > 1) {
			new AlertDialog.Builder(this).setItems(
					results.toArray(new String[results.size()]),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							mFromTextEditText.setText(results.get(which));
							startTranslate();
						}
					}).show();
		} else {
			mFromTextEditText.setText(results.get(0));
			startTranslate();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if (parent == mToLanguageSpinner) {
			startTranslate();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

	public static boolean isNetworkConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		if (!mp.isPlaying()) {
			mp.start();
		}
		mTranslateImageButton.setVisibility(View.VISIBLE);
		mProgressBar.setVisibility(View.GONE);
	}
}
